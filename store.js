#!/usr/bin/env node
'use strict';
const minimist = require('minimist');
var Dict = require('./Dictionary');
let args = minimist(process.argv.slice(2));
var newDic = new Dict('myDictionary.txt');
if (args["_"].length) {
    switch (args["_"][0]) {
        case "add":
            if (args["_"][1] && args["_"][2]) {
                newDic.addElement(args["_"][1], args["_"][2]);
                console.log("Added Successfully");
            } else {
                console.log("Missing input");
            }
            break;
        case "list":
            console.log(newDic.listElements());
            break;
        case "get":
            if (args["_"][1]) {
                var result = newDic.getElementByKey(args["_"][1]);
                if (result) {
                    console.log(result);
                } else {
                    console.log("No matched key");
                }
            } else {
                console.log("Missing input");
            }
            break;
        case "remove":
            if (args["_"][1]) {
                if (newDic.removeElementByKey(args["_"][1])) {
                    console.log("Deleted");
                } else {
                    console.log("No matched key");
                }
            } else {
                console.log("Missing input");
            }
            break;
        case "clear":
            newDic.clearAllElements();
            console.log("Dictionary now empty");
            break;
        default:
            console.log("choose a valide opertion from :\n add \n list \n remove \n clear");
            break;
    }
} else {
    console.log("Please enter opertaion");
}