var fs = require('fs');
class Dictionary {
    constructor(_fileName) {
        this._fileName=_fileName;
        this._myDictionary = {};
        this._myDictionary = JSON.parse(fs.readFileSync(this._fileName, 'utf8'))
    }
    addElement(_key, _value) {
        this._myDictionary[_key] = _value;
        this.syncFile();
    }
    listElements() {
        return this._myDictionary;
    }
    getElementByKey(_key) {
        return this._myDictionary[_key];
    }
    removeElementByKey(_key) {
        var status=0;
        Object.keys(this._myDictionary).forEach((elem, index) => {
            if (elem == _key) {
                delete this._myDictionary[elem];
                this.syncFile();
                status= 1 ;
            }
            if(Object.keys(this._myDictionary).length -1 == index ){
                status = 0;
            }
        });
        return status;
    }
    clearAllElements() {
        this._myDictionary = {};
        this.syncFile("./myDictionary.txt");
    }
    syncFile() {
        fs.writeFileSync(this._fileName, JSON.stringify(this.listElements()), 'utf-8');
    }
}
module.exports = Dictionary;